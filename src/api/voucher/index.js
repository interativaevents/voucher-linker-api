import { Router } from 'express'
import { middleware as query } from 'querymen'
import { middleware as body } from 'bodymen'
import { create, index, show, update, destroy, link } from './controller'
import { schema } from './model'
export Voucher, { schema } from './model'

const router = new Router()
const { project, itemId, voucher, activated } = schema.tree

/**
 * @api {post} /vouchers Create voucher
 * @apiName CreateVoucher
 * @apiGroup Voucher
 * @apiParam project Voucher's project.
 * @apiParam itemId Voucher's itemId.
 * @apiParam voucher Voucher's voucher.
 * @apiParam activated Voucher's activated.
 * @apiSuccess {Object} voucher Voucher's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Voucher not found.
 */
router.post('/',
  body({ itemId,activated:true }),
  link)

/**
 * @api {get} /vouchers Retrieve vouchers
 * @apiName RetrieveVouchers
 * @apiGroup Voucher
 * @apiUse listParams
 * @apiSuccess {Object[]} vouchers List of vouchers.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 */
router.get('/',
  query({itemId:String,voucher:String}),
  index)

/**
 * @api {get} /vouchers/:id Retrieve voucher
 * @apiName RetrieveVoucher
 * @apiGroup Voucher
 * @apiSuccess {Object} voucher Voucher's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Voucher not found.
 */
router.get('/:id',
  show)

/**
 * @api {put} /vouchers/:id Update voucher
 * @apiName UpdateVoucher
 * @apiGroup Voucher
 * @apiParam project Voucher's project.
 * @apiParam itemId Voucher's itemId.
 * @apiParam voucher Voucher's voucher.
 * @apiParam activated Voucher's activated.
 * @apiSuccess {Object} voucher Voucher's data.
 * @apiError {Object} 400 Some parameters may contain invalid values.
 * @apiError 404 Voucher not found.
 */
router.put('/:id',
  body({ project, itemId, voucher, activated }),
  update)

/**
 * @api {delete} /vouchers/:id Delete voucher
 * @apiName DeleteVoucher
 * @apiGroup Voucher
 * @apiSuccess (Success 204) 204 No Content.
 * @apiError 404 Voucher not found.
 */
router.delete('/:id',
  destroy)

export default router
