import mongoose, { Schema } from 'mongoose'

const voucherSchema = new Schema({ 
  itemId: {
    type: String
  },
  voucher: {
    type: String
  },
  activated: {
    type: Boolean
  }
}, {
  timestamps: true
})

voucherSchema.methods = {
  view (full) {
    const view = {
      // simple view
      id: this.id,      
      itemId: this.itemId,
      voucher: this.voucher,
      activated: this.activated,
      createdAt: this.createdAt,
      updatedAt: this.updatedAt
    }

    return full ? {
      ...view
      // add properties for a full view
    } : view
  }
}

const model = mongoose.model('Voucher', voucherSchema)

export const schema = model.schema
export default model
