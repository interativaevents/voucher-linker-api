import { Voucher } from '.'

let voucher

beforeEach(async () => {
  voucher = await Voucher.create({ project: 'test', itemid: 'test', voucher: 'test', activated: 'test' })
})

describe('view', () => {
  it('returns simple view', () => {
    const view = voucher.view()
    expect(typeof view).toBe('object')
    expect(view.id).toBe(voucher.id)
    expect(view.project).toBe(voucher.project)
    expect(view.itemid).toBe(voucher.itemid)
    expect(view.voucher).toBe(voucher.voucher)
    expect(view.activated).toBe(voucher.activated)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })

  it('returns full view', () => {
    const view = voucher.view(true)
    expect(typeof view).toBe('object')
    expect(view.id).toBe(voucher.id)
    expect(view.project).toBe(voucher.project)
    expect(view.itemid).toBe(voucher.itemid)
    expect(view.voucher).toBe(voucher.voucher)
    expect(view.activated).toBe(voucher.activated)
    expect(view.createdAt).toBeTruthy()
    expect(view.updatedAt).toBeTruthy()
  })
})
