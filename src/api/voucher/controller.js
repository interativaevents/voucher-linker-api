import _ from 'lodash'
import { success, notFound } from '../../services/response/'
import { Voucher } from '.'

export const create = ({ bodymen: { body } }, res, next) =>
  Voucher.create(body)
    .then((voucher) => voucher.view(true))
    .then(success(res, 201))
    .catch(next)

export const index = ({ querymen: { query, select, cursor } }, res, next) => {
  console.log(query)
  return Voucher.find(query, select, cursor)
    .then((vouchers) => vouchers.map((voucher) => voucher.view()))
    .then(success(res))
    .catch(next)
}

export const show = ({ params }, res, next) =>
  Voucher.findById(params.id)
    .then(notFound(res))
    .then((voucher) => voucher ? voucher.view() : null)
    .then(success(res))
    .catch(next)

export const update = ({ bodymen: { body }, params }, res, next) =>
  Voucher.findById(params.id)
    .then(notFound(res))
    .then((voucher) => voucher ? _.merge(voucher, body).save() : null)
    .then((voucher) => voucher ? voucher.view(true) : null)
    .then(success(res))
    .catch(next)

export const destroy = ({ params }, res, next) =>
  Voucher.findById(params.id)
    .then(notFound(res))
    .then((voucher) => voucher ? voucher.remove() : null)
    .then(success(res, 204))
    .catch(next)

export const link = ({ bodymen: { body } }, res, next) => {
  console.log(body);
  Voucher.findOne({ activated: false })
    .then(notFound(res))
    .then((voucher) => voucher ? _.merge(voucher, body).save() : null)
    .then((voucher) => voucher ? voucher.view(true) : null)
    .then(success(res))
    .catch(next)
}