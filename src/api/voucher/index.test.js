import request from 'supertest-as-promised'
import express from '../../services/express'
import routes, { Voucher } from '.'

const app = () => express(routes)

let voucher

beforeEach(async () => {
  voucher = await Voucher.create({})
})

test('POST /vouchers 201', async () => {
  const { status, body } = await request(app())
    .post('/')
    .send({ project: 'test', itemid: 'test', voucher: 'test', activated: 'test' })
  expect(status).toBe(201)
  expect(typeof body).toEqual('object')
  expect(body.project).toEqual('test')
  expect(body.itemid).toEqual('test')
  expect(body.voucher).toEqual('test')
  expect(body.activated).toEqual('test')
})

test('GET /vouchers 200', async () => {
  const { status, body } = await request(app())
    .get('/')
  expect(status).toBe(200)
  expect(Array.isArray(body)).toBe(true)
})

test('GET /vouchers/:id 200', async () => {
  const { status, body } = await request(app())
    .get(`/${voucher.id}`)
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(voucher.id)
})

test('GET /vouchers/:id 404', async () => {
  const { status } = await request(app())
    .get('/123456789098765432123456')
  expect(status).toBe(404)
})

test('PUT /vouchers/:id 200', async () => {
  const { status, body } = await request(app())
    .put(`/${voucher.id}`)
    .send({ project: 'test', itemid: 'test', voucher: 'test', activated: 'test' })
  expect(status).toBe(200)
  expect(typeof body).toEqual('object')
  expect(body.id).toEqual(voucher.id)
  expect(body.project).toEqual('test')
  expect(body.itemid).toEqual('test')
  expect(body.voucher).toEqual('test')
  expect(body.activated).toEqual('test')
})

test('PUT /vouchers/:id 404', async () => {
  const { status } = await request(app())
    .put('/123456789098765432123456')
    .send({ project: 'test', itemid: 'test', voucher: 'test', activated: 'test' })
  expect(status).toBe(404)
})

test('DELETE /vouchers/:id 204', async () => {
  const { status } = await request(app())
    .delete(`/${voucher.id}`)
  expect(status).toBe(204)
})

test('DELETE /vouchers/:id 404', async () => {
  const { status } = await request(app())
    .delete('/123456789098765432123456')
  expect(status).toBe(404)
})
